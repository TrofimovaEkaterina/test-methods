package ru.nsu.fit.endpoint.service.manager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;

import java.util.UUID;

public class CustomerManagerTest {
    private DBService dbService;
    private Logger logger;
    private CustomerManager customerManager;

    private Customer customerBeforeCreateMethod;
    private Customer customerAfterCreateMethod;

    @Before
    public void before() {
        // create stubs for the test's class
        dbService = Mockito.mock(DBService.class);
        logger = Mockito.mock(Logger.class);

        customerBeforeCreateMethod = new Customer()
                .setId(null)
                .setFirstName("John")
                .setLastName("Wick")
                .setLogin("john_wick@gmail.com")
                .setPass("Baba_Jaga")
                .setBalance(0);
        customerAfterCreateMethod = customerBeforeCreateMethod.clone();
        customerAfterCreateMethod.setId(UUID.randomUUID());

        Mockito.when(dbService.createCustomer(customerBeforeCreateMethod)).thenReturn(customerAfterCreateMethod);

        // create the test's class
        customerManager = new CustomerManager(dbService, logger);
    }

    @Test
    public void testCreateNewCustomer() {
        // Вызываем метод, который хотим протестировать
        Customer customer = customerManager.createCustomer(customerBeforeCreateMethod);

        // Проверяем результат выполенния метода
        Assert.assertEquals(customer.getId(), customerAfterCreateMethod.getId());

        // Проверяем, что метод мока базы данных был вызван 1 раз
        Assert.assertEquals(1, Mockito.mockingDetails(dbService).getInvocations().size());
    }

    @Test
    public void testCreateCustomerWithNullArgument() {
        try {
            customerManager.createCustomer(null);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Argument 'customerData' is null.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithEasyPassword() {
        try {
            customerBeforeCreateMethod.setPass("123qwe");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Password is easy.", ex.getMessage());
        }
    }

    /////////////////////////////////////////////////////////////////////

    @Test
    public void testCreateCustomerWithShortFirstName() {
        try {
            customerBeforeCreateMethod.setFirstName("A");
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail("No expected exception");
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("First name's length should be more or equal 2 symbols and less or equal 12 symbols.", ex.getMessage());
        }
    }


    @Test
    public void testCreateCustomerWithLongFirstName() {
        try {
            customerBeforeCreateMethod.setFirstName("Aaaaaaaaaaaaa");
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail("No expected exception");
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("First name's length should be more or equal 2 symbols and less or equal 12 symbols.", ex.getMessage());
        }
    }


    @Test
    public void testCreateCustomerWithLowerBoundLengthFirstName() {
            customerBeforeCreateMethod.setFirstName("Aa");
            customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test
    public void testCreateCustomerWithUpperBoundLengthFirstName() {
        customerBeforeCreateMethod.setFirstName("Aaaaaaaaaaaa");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test
    public void testCreateCustomerWithFirstNameStartsWithLowCase() {
        try{
            customerBeforeCreateMethod.setFirstName("john");
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail("No expected exception");
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("First name should start with character in upper case", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithShortLastName() {
        try {
            customerBeforeCreateMethod.setLastName("A");
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail("No expected exception");
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Last name's length should be more or equal 2 symbols and less or equal 12 symbols.", ex.getMessage());
        }
    }


    @Test
    public void testCreateCustomerWithLongLastName() {
        try {
            customerBeforeCreateMethod.setLastName("Aaaaaaaaaaaaa");
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail("No expected exception");
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Last name's length should be more or equal 2 symbols and less or equal 12 symbols.", ex.getMessage());
        }
    }


    @Test
    public void testCreateCustomerWithLowerBoundLengthLastName() {
        customerBeforeCreateMethod.setLastName("Aa");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test
    public void testCreateCustomerWithUpperBoundLengthLastName() {
        customerBeforeCreateMethod.setLastName("Aaaaaaaaaaaa");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test
    public void testCreateCustomerWithLastNameStartsWithLowCase() {
        try{
            customerBeforeCreateMethod.setLastName("john");
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail("No expected exception");
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Last name should start with character in upper case", ex.getMessage());
        }
    }


}
